import java.util.Scanner;

public class Calculadora { // classe calculadora

    public static void main(String[] args) { // método principal

        calculadora(); // método
    }

    public static void calculadora() { //método calculadora

        Scanner in = new Scanner(System.in);
        int num1, num2, soma, subtracao, multiplicacao, divisao; //declaração de variáveis

        System.out.println("Insira um valor: ");
        num1 = in.nextInt();

        System.out.println("Insira outro valor: ");
        num2 = in.nextInt();

        soma = num1 + num2;
        System.out.println("O valor total da soma é: " + soma);

        subtracao = num1 - num2;
        System.out.println("O valor total da subtração é: " + subtracao);

        multiplicacao = num1 * num2;
        System.out.println("O valor total da multiplicação é: " + multiplicacao);

        divisao = num1 / num2;
        System.out.println("O valor total da divisão é: " + divisao);

    }
}
